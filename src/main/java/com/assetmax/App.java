package com.assetmax;

import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContexts;
import org.json.JSONObject;

/**
 * Very basic example that shows how to use Assetmax's APIs.
 */
public class App {
//  static final String SERVER_URL = "https://demo2.assetmax.ch";
  static final String SERVER_URL = "https://local.assetmax.ch";
  static final String API_URL = SERVER_URL + "/assetmax/moik/ext";
  static final String API_USER_EMAIL = "api-example@assetmax.ch";
  static final String API_USER_PWD = "api-ex!--f";

  public static void main(String[] args) throws Exception {

    // add support for self signed certificates
    HttpClientBuilder builder = HttpClients.custom()
      .setSSLSocketFactory(new SSLConnectionSocketFactory(
        SSLContexts.custom()
          .loadTrustMaterial(null, new TrustSelfSignedStrategy())
          .build()
      ));
    Executor executor = Executor.newInstance(builder.build());

    // 1) login using email and password
    Content content = executor.execute(
      Request.Post(getApiUrl("login", "login"))
        .addHeader("X-Requested-With", "XMLHttpRequest")
        .bodyForm(
          new BasicNameValuePair("email", API_USER_EMAIL),
          new BasicNameValuePair("password", API_USER_PWD),
          new BasicNameValuePair("auth", "emailpassword")
        ))
      .returnContent();

    // 2) extract session token
    JSONObject response = new JSONObject(content.asString());
    String tokenId = response.getString("tokenId");

    // 3) ask for all equity instruments
    content = executor.execute(
      Request.Post(getApiUrl("instrument", "get-financial-instruments"))
        .addHeader("Cookie", "tokenId=" + tokenId)
        .addHeader("X-Requested-With", "XMLHttpRequest")
        .bodyForm(
          new BasicNameValuePair("icomp_type", "stock")
        ))
      .returnContent();
    response = new JSONObject(content.asString());
    System.out.println("Found " + response.getInt("total") + " stock instruments");
  }

  private static String getApiUrl(String serviceId, String methodId) {
    return API_URL + "/" + serviceId + "/" + methodId;
  }
}
